import { Bento, VariableFileLoader } from '@ayanaware/bento';
import { Bentocord } from '@ayanaware/bentocord';

import { Community } from '../src/Community';

import { LocalizationStub } from './LocalizationStub';

const bento = new Bento();

(async () => {
	const vfl = new VariableFileLoader();
	await vfl.addFile([__dirname, '..', '..', 'env.example.json'], true);
	await vfl.addFile([__dirname, '..', '..', 'env.json']);

	await bento.addPlugins([vfl, Bentocord, Community, LocalizationStub]);

	await bento.verify();
})().catch(e => {
	console.error(e);
	process.exit(1);
});
