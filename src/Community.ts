import { Component, Plugin, PluginAPI } from '@ayanaware/bento';

import { Tableflip } from './commands/Tableflip';

export class Community implements Plugin {
	public name = '@ayana/community';
	public api!: PluginAPI;

	private readonly entities: Array<Component> = [
		Tableflip,
	];

	public async onLoad(): Promise<unknown> {
		const entityManager = this.api.bento.entities;

		return entityManager.addComponents(this.entities);
	}

	public async onUnload(): Promise<void> {
		const entityManager = this.api.bento.entities;

		for (const entity of this.entities) await entityManager.removeComponent(entity);
	}
}
