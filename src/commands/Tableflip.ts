import { ComponentAPI } from '@ayanaware/bento';
import { CommandContext, CommandDefinition, CommandEntity, CommandManager } from '@ayanaware/bentocord';

export class Tableflip implements CommandEntity {
	public name = '@ayana/community:Tableflip';
	public api!: ComponentAPI;
	public parent = CommandManager;

	public definition: CommandDefinition = {
		aliases: ['tableflip', 'flip'],
		description: { key: 'COMMUNITY_DESCRIPTION_TABLEFLIP' },
	};

	private readonly flips = [
		' (╯°□°）╯︵ ┻━┻',
		'(┛◉Д◉)┛彡┻━┻',
		'(ﾉ≧∇≦)ﾉ ﾐ ┸━┸',
		'(ノಠ益ಠ)ノ彡┻━┻',
		'(╯ರ ~ ರ）╯︵ ┻━┻',
		'(┛ಸ_ಸ)┛彡┻━┻',
		'(ﾉ´･ω･)ﾉ ﾐ ┸━┸',
		'(ノಥ,_｣ಥ)ノ彡┻━┻',
		'(┛✧Д✧))┛彡┻━┻',
	];

	public async execute(ctx: CommandContext): Promise<unknown> {
		const flip = this.flips[Math.floor(Math.random() * this.flips.length)];
		return ctx.createResponse(flip);
	}
}
