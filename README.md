## Ayana Community
This repository is for those community members who really want to see a feature in Ayana and have the skills to implement it themselves.
Most things in this repo will be limited in scope to Commands. However if you have the knowledge to build a full system it will be reviewed seriously.

Merge Requests are more then welcome they are the entire point of this repository. If accepted your code will be made available in Ayana herself,
provided and discoverable to the thousands of users using her daily. This repo has everything you need to start building and testing.

## Bento & Bentocord
Ayana uses [Bento](https://gitlab.com/ayanaware/bento) & [Bentocord](https://gitlab.com/ayanaware/bentocord) for modules, commands, and more. Please be intimately familar with both of these before submitting Merge Requests.

## Testing
Ayana herself is a proprietary codebase, however because of the Open source projects we maintain like [Bento](https://gitlab.com/ayanaware/bento) & [Bentocord](https://gitlab.com/ayanaware/bentocord) it is easy to create a very basic bot that is capable of running the community features until they end up in Ayana. While the enviroments are not exactly the same, things are close enough that if it works with the test bot it should work in Ayana as well. 

As time goes on more features will be made available to the community project. Such as persistance, cache, IPC. If you need these now
feel free to check if an issue was already opened or open one yourself requesting it.

To actually run the test instance of Ayana Community:
- Ensure you have set your `BENTOCORD_TOKEN` in `env.json`
- Execute `yarn run build` to build or `yarn run debug` to build and run test instance

## Contributing
When you contribute here you are yeilding your copyright to the code you contribute, and ensuring that we will continue to be able to use, modify, or remove it going forward.